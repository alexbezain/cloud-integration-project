package Efrei.com.Microservice1;

import javax.persistence.*;

@Entity // This tells Hibernate to make a table out of this class


public class Actor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private int id;

    private String name;

    private String birthDate;

    private String movieName;

    public Actor(int id, String name, String birthDate, String movieName) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.movieName = movieName;

    }

    public Actor (){
    }

    public int getId(){
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getMovieName(){
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }
}