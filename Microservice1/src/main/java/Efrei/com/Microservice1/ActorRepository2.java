package Efrei.com.Microservice1;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ActorRepository2 extends JpaRepository<Actor, Integer>, JpaSpecificationExecutor<Actor> {

}