package dtcs.oa.efrei;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;


@Service

public class ActorService {

    private final CircuitBreakerFactory circuitBreakerFactory;
    final Logger LOGGER = LoggerFactory.getLogger(ActorService.class);

    public ActorService(CircuitBreakerFactory circuitBreakerFactory) {
        this.circuitBreakerFactory = circuitBreakerFactory;
    }

    public List<Actor> actorService(String movieTitle) {

            var restTemplate = new RestTemplate();

            return circuitBreakerFactory.create("getMovieActors").run(
                    () -> restTemplate.exchange(
                            "http://localhost:8081/getSpecificActor?search=movieName:'" + movieTitle +"'",
                            HttpMethod.GET,
                            null,
                            new ParameterizedTypeReference<List<Actor>>() {
                            }).getBody(),
                    t -> {
                        LOGGER.error("getMovieActors call failed",t);
                        System.out.println("http://localhost:8081/getSpecificActor?search=movieName:'" + movieTitle +"'");
                        return defaultActors();
                    }
            );
    }

    private static List<Actor> defaultActors() {
        return List.of(
                new Actor(0,"John Doe", "10/01/2010", "None")
        );
    }
};
